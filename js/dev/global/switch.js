$(document).ready(function () {

  switchInit();

});

function switchInit() {
  if ($('.js--switch').length) {
    $('.js--switch').each(function () {
      var $this = $(this);

      var $on = $this.parent().find('.js--switch__el--on');
      var $off = $this.parent().find('.js--switch__el--off');
      var width = Math.max($on.outerWidth(), $off.outerWidth());
      var test = $on.width() - $off.width();

      $on.css('min-width', width);
      $off.css('min-width', width);

      checkSwitchState($this);

    });
  }
}

$(document).on('change', '.js--switch', function () {
  changeSwitchState($(this));
});

function switchOn(el) {
  el.prop('checked', true).attr("checked", "checked").attr("value", "1").checked = true;
  el.parent().find('.switch__ui').addClass('--on');
}

function switchOff(el) {
  el.prop('checked', false).removeAttr('checked').attr("value", "0").checked = false;
  el.parent().find('.switch__ui').removeClass('--on');
}

function checkSwitchState(el) {
  var $switch = $(el);
  if ($switch.attr('checked')) {
    switchOn($switch);
  } else {
    switchOff($switch);
  }
}

function changeSwitchState(el) {
  if (el.attr('checked')) {
    switchOff(el);
  } else {
    switchOn(el);
  }
}

$('button').click(function () {
  changeSwitchState($('#' + $(this).data('switch')));
});